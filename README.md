# User metrics (Rolling Retention 7 day and llifespan histogram)

## To install
**For windows, assuming that you have git & yarn installed**

1. Open powershell and use `cd "the directory where you want to copy the app to"`
2. `git clone https://gitlab.com/demonpiontb/user-metrics.git`
3. `cd ./user-metrics` to move to app dir
4. `yarn` to add yarn scripts
5. `yarn start` to launch app
**Note: two minimized instances of powershell would be launched. Close them with ^C to stop the app.**

## To do:

1. Saving different data sets function