import React, { useState, useEffect } from 'react';
import Histogram from './Histogram';
import Popup from 'reactjs-popup';

const Metrics = (props) => {

    const [openPopup, setOpenPopup] = useState(false);

    const closePopup = () => {
        setOpenPopup(false);
    };

    useEffect(() => {
        if (props.data !== undefined) {
            if (props.data.error === true) {
                setOpenPopup(o => !o);
            }
        }
    }, [props.data]);

    return(
        <div>
            {props.data !== undefined && props.data.error !== true ?
            <div>           
                <div className='metrics-text'>
                    Rolling retention 7 day = {props.data.retention_7.toFixed(2)}% <br /><br />
                    User lifespan histogram
                </div>
                <div>
                    <Histogram data={props.data.lifespanData}/>
                </div>
            </div>
            :
            <div className='metrics-text'>
                Press "calculate" to see metrics
            </div>
            }
            <div>
                <Popup open={openPopup} closeOnDocumentClick onClose={closePopup}>
                    <div className='popup-message'>
                        No data written!
                    </div>
                    <button className="popup-action-button" onClick={() => {closePopup()}}>OK</button>
                </Popup>
            </div>
        </div>
    )
}

export default Metrics;
