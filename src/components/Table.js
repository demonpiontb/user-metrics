import React, { useState, useEffect } from 'react';
import Metrics from '../components/Metrics';
import metrics from '../utils/metrics';
import customStyles from '../styles/tableStyles';
import DataTable from 'react-data-table-component';

var dataStorage = [];       //Data used only to control amount of rows and is STATE for rerender when rows change.
                            //DataStorage is a variable and actualy contains data.
const Table = (props) => {

    const [rowAmount, setRowAmount] = useState(1);
    const [data, setData] = useState([]);
    const [metricsData, setMetricsData] = useState(undefined);

    const rowController = (rowAmount) => {
        let tempArray = [];
        for (let i = 0; i < rowAmount; i++) {
            tempArray[i] = {id: i};
            if (!dataStorage[i]) {
                dataStorage[i] = {id: i};
            }
        }
        if (tempArray.length < dataStorage.length) {
            dataStorage.pop();
        }
        setData(tempArray);
    }

    const handleInputChange = (e) => {
        let tempArray = dataStorage.slice();
        let rowIndex = tempArray.findIndex(row => row.id.toString() === e.target.name.split("_")[1]);
        switch (e.target.name.split("_")[0]) {
            case "userID":
                tempArray[rowIndex].userID = e.target.value;
                break;
            case "date-registration":
                tempArray[rowIndex].dateRegistration = e.target.value;
                break;
            case "date-last-activity":
                tempArray[rowIndex].dateLastActivity = e.target.value;
                break;
        }
        dataStorage = tempArray;
    }

    const addRow = () => {
        setRowAmount(rowAmount + 1);
    }

    const deleteRow = () => {
        rowAmount > 1 ? setRowAmount(rowAmount - 1) : console.log("No can do");
    }

    const calculateMetrics = async() => {
        let metricsData = await metrics.calculateMetrics();
        setMetricsData(metricsData);
    }

    const columns = [
        {
            name: 'User ID',
            cell:(row) => <input type="text" className="text-input" name={"userID_"+ row.id} required defaultValue={row.userID} onChange={handleInputChange}/>,
        },
        {
            name: 'Date Registration',
            cell:(row) => <input type="date" className="date-picker" name={"date-registration_"+ row.id} max={new Date().toISOString().substring(0, 10)} required onChange={handleInputChange} />,
        },
        {
            name: 'Date Last Activity',
            cell:(row) => <input type="date" className="date-picker" name={"date-last-activity_"+ row.id} max={new Date().toISOString().substring(0, 10)} required onChange={handleInputChange} />,
        }
    ];

    useEffect( () => {
        rowController(rowAmount);
    }, [rowAmount])

    return(
        <div>
            <DataTable
                customStyles={customStyles}
                columns={columns}
                data={data}
            />
            <div className='buttons-wrapper'>
                <button className='action-button' onClick={() => {props.saveData(dataStorage)}}>SAVE DATA</button>
                <button className='action-button' onClick={calculateMetrics}>CALCULATE</button>
                <button className='action-button' onClick={addRow}>ADD ROW</button>
                <button className='action-button' onClick={deleteRow}>DELETE ROW</button>
            </div>
            <div className='metrics-wrapper'>
                <Metrics data={metricsData}/>
            </div>
        </div>
    )
}

export default Table;
