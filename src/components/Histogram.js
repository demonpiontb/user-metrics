import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend
} from "recharts";

export default function Histogram(props) {

    var data = [];

    Object.keys(props.data).forEach((key) => {
        data.push({
            name: key == 1 ? key + " day" : key + " days",
            users: props.data[key]
        });
    })

    return (
        <BarChart
        width={700}
        height={300}
        data={data}
        margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5
        }}
        >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis allowDecimals = {false} label={{ value: 'Users amount', angle: -90, position: 'insideLeft' }} />
        <Tooltip />
        <Legend />
        <Bar name="Amount of users" dataKey="users" fill="#8884d8" />
        </BarChart>
    );
}