import dataUtil from "./data";

const metrics = {
    calculateMetrics: async() => {
        let dataToPass = {};
        let data = await dataUtil.getData();
        let returnedAfter_7 = 0;
        let registeredBefore_7 = 0;
        let retention_7;
        let lifespanData = [];
        if (data.usersData.length > 0) {
            data.usersData.forEach((user) => {
                let lastActivity = new Date(user.dateLastActivity);
                let registered = new Date(user.dateRegistration);
                let lifespanInDays = (lastActivity - registered)/(1000*60*60*24) + 1; //Not sure how days are counted in such metrics, if last day of activity should not be included - remove "+ 1".
                if (lifespanInDays >= 7) {
                    returnedAfter_7 += 1;
                }
                if((Date.now() - registered)/(1000*60*60*24) >= 7) {
                    registeredBefore_7 += 1;
                }
                lifespanData[lifespanInDays] ? lifespanData[lifespanInDays] += 1 : lifespanData[lifespanInDays] = 1;
            })
            for (let i = 1; i < lifespanData.length; i++) {
                if (lifespanData[i] === undefined) {
                    lifespanData[i] = 0;
                }
            }
            registeredBefore_7 ? retention_7 = (returnedAfter_7 / registeredBefore_7) * 100 : retention_7 = 0; //Rule out the possibility of zero division
            dataToPass = {
                retention_7: retention_7,
                lifespanData: lifespanData,
            };
        } else {
            dataToPass = {
                error: true,
            };
        }
        return dataToPass;
    }
}

export default metrics