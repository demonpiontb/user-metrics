const axios = require('axios');

const dataUtil = {
    getData: async() => {
        const { data } = await axios.get('http://localhost:3001/data/1/');
        return data
    },
    writeData: async(resourceName, data) => {
        await axios.delete(`http://localhost:3001/data/1/`);
        axios.post('http://localhost:3001/data/', {
            id: 1,
            resourceName: resourceName,    //I intended to make "save system", where you can also load data by resource name,
            usersData: data                //but there is an issue with duplicating resource names.
        });                                //And as it is not a part of task, i left it as it is, though it could be done if needed.
    },
    validate: (data) => {
        let tempValidation = true;
        let rowNumber = 0;
        data.forEach((user) => {
            rowNumber += 1;
            if (!user.userID) {
                tempValidation += `\nFill user ID in row ${rowNumber}`;
            }
            if (!user.dateRegistration) {
                tempValidation += `\nFill date registration in row ${rowNumber}`;
            }
            if (!user.dateLastActivity) {
                tempValidation += `\nFill date last activity in row ${rowNumber}`;
            }
            if (new Date(user.dateLastActivity) < new Date(user.dateRegistration)) {
                tempValidation += `\nLast activity should be later or same as registration date in row ${rowNumber}`;
            }
        })
        return tempValidation
    }
}

export default dataUtil;