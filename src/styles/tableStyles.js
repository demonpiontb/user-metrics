const customStyles = {
    rows: {
        style: {
            minHeight: '28px',
            background: "#F9F9F9",
            borderBottomColor: "#FFFFFF",
            outline: "1px solid #F9F9F9",
        },
    },
    headCells: {
        style: {
            fontStyle: "normal",
            fontWeight: "400",
            fontSize: "14px",
            lineHeight: "16px",
            textTransform: "uppercase",
            color: "#3C5AA866",
            height: "45px",
            background: "#F9F9F9",
        },
    },
    headRow: {
        style: {
            backgroundColor: "#F9F9F9",
            minHeight: '52px',
            borderBottomWidth: '1px',
            borderBottomColor: "red",
            borderBottomStyle: 'solid',
        },
    },
    cells: {
        style: {
            background: "#F9F9F9",
        },
    },
};

export default customStyles;