import React, { useState, useEffect } from 'react';
import Table from '../components/Table';
import dataUtil from '../utils/data';
import Popup from 'reactjs-popup';

export default function MainPage() {
    const [data, setData] = useState();
    const [openPopup, setOpenPopup] = useState(false);
    const [validation, setValidation] = useState(undefined);

    const closePopup = () => {
        setOpenPopup(false);
        setValidation(undefined);
    };
    
    const saveData = () => {
        dataUtil.writeData("test", data) ;   //Check "../utils/data" to see why "test" is here.
    }

    const dataValidation = (data) => {
        setData(data);
        let tempValidation = true;
        tempValidation = dataUtil.validate(data);
        setValidation(tempValidation);
    }

    useEffect(() => {
        if (validation !== undefined) {
            if (validation === true) {
                setOpenPopup(o => !o);
                saveData();
            } else {
                setOpenPopup(o => !o);
            }
        }
    }, [validation]);

    return(
        <div className='content-wrapper'>
            <div className='table-wrapper'>
                <Table saveData={dataValidation} />
            </div>
            <div>
                <Popup open={openPopup} closeOnDocumentClick onClose={closePopup}>
                    <div className='popup-message'>
                        {validation === undefined ? 
                        validation :
                        validation === true ?
                        <>Data saved!</> :
                        validation.replace("true", "Fix following issues:")}
                    </div>
                    <button className="popup-action-button" onClick={() => {closePopup()}}>OK</button>
                </Popup>
            </div>
        </div>
    )
}
